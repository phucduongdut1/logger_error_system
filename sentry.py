import sentry_sdk

sentry_sdk.init(
    "https://0551e10c6811444488bcc81ef639ae86@o501859.ingest.sentry.io/5661064",
    traces_sample_rate=1.0
)

try:
    division_by_zero = 1 / 0
except Exception as e:
    sentry_sdk.capture_message('division by zero message')
    sentry_sdk.capture_exception(e)

