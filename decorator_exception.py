import sentry_sdk
import logging
import time
from modules.slack_message import SlackMessage
from dotenv import load_dotenv
import os
from datetime import datetime


logging.basicConfig(level=logging.INFO)

load_dotenv()

sentry_sdk.init(
    os.getenv('SENTRY_LINK'),
    traces_sample_rate=1.0
)

slack_message = SlackMessage()
slack_message.change_attrs(url_post_message=os.getenv('URL_POST_MESSAGE'), channel_id=os.getenv("CHANNEL_ID"), token=os.getenv("TOKEN"))


def exception_handler(*args, **kwargs):
    def run_exception_handler(func):
        def inner_function(*args, **kwargs):
            try:
                start = datetime.now()
                text_start = "func {func_name} start running at {time}".format(func_name=func.__name__, time=start.strftime("%m/%d/%Y, %H:%M:%S"))
                logging.info(text_start)
                response = slack_message.post_message(text_start)
                ts, id_channel = slack_message.get_reply_params(response)

                func(*args, **kwargs)

                text_reply = "{funtion_name} finished with time: {time}".format(funtion_name=func.__name__, time=str(datetime.now() - start))
                slack_message.reply_message(ts, id_channel, text_reply, False)
                logging.info(text_reply)

            except Exception as e:
                text_reply = f'func {func.__name__} have error: ' + e.args[0]
                slack_message.reply_message(ts, id_channel, text_reply, True)
                logging.error(text_reply)

                sentry_sdk.capture_exception(e)

        return inner_function

    return run_exception_handler


@exception_handler(code="avb", tracking=True)
def area_square(length):
    logging.info(str(length * length))


@exception_handler()
def area_circle(radius):
    logging.info(str(3.14 * radius * radius))


@exception_handler()
def area_rectangle(length, breadth):
    logging.info(str(length * breadth))


area_square(2)
area_circle(2)
area_rectangle(2, 4)
area_square("some_str")
area_circle("some_other_str")
area_rectangle("some_other_rectangle")

