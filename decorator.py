def decorators(*args, **kwargs):
    def inner(func):
        for arg in args:
            print(arg)
        func()
        print('end')

    return inner  # this is the fun_obj mentioned in the above content


@decorators(1, 2, 3)
def func(a):
    print(a)

func('phuc')