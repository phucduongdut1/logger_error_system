import requests

class SlackMessage:
    _instance = None

    url_get_conversation = None
    url_post_message = None
    channel_id = None
    token = None

    def __init__(self):
        if SlackMessage._instance is None:
            SlackMessage._instance = self
        else:
            return SlackMessage._instance

    @staticmethod
    def change_attrs(url_get_conversation=None, url_post_message=None, channel_id=None, token=None):
        SlackMessage._instance.url_get_conversation = url_get_conversation
        SlackMessage._instance.url_post_message = url_post_message
        SlackMessage._instance.channel_id = channel_id
        SlackMessage._instance.token = token

    @staticmethod
    def get_instance():
        if SlackMessage._instance is None:
            SlackMessage()
        else:
            return SlackMessage._instance

    def post_message(self, text):
        headers = {"Authorization": "Bearer {token}".format(token=self.token)}
        data = {
            "channel": self.channel_id,
            "text": text
        }

        response = requests.post(url=self.url_post_message, headers=headers, data=data)
        return response

    def reply_message(self, ts, id_channel, text, reply_broadcast):
        url = self.url_post_message
        headers = {"Authorization": "Bearer {token}".format(token=self.token)}

        data = {
            "channel": id_channel,
            "thread_ts": ts,
            "text": text,
            "reply_broadcast": reply_broadcast
        }

        response = requests.post(url=url, headers=headers, data=data)
        return response

    def get_conversation(self):
        headers = {"Authorization": "Bearer {token}".format(token=self.token)}
        response = requests.get(url=self.url_get_conversation, headers=headers)

        return response

    @staticmethod
    def get_reply_params(response):
        ts = response.json()['ts']
        id_channel = response.json()['channel']
        return ts, id_channel

